import React, { Component } from 'react';


class Footer extends Component {
  render() {
    return (
     
      <footer>
          <h3>SPARC CCI</h3>
          <p>Contact US <br/> ccisparc@fisat.ac.in</p>
          <ul>
            <li><a href="#"><i className="fab fa-facebook-f"></i></a></li>
            <li><a href="#"><i className="fab fa-twitter"></i></a></li>
            <li><a href="#"><i className="fab fa-instagram"></i></a></li>
          </ul>
      </footer>
      
    );
  }
}

export default Footer;